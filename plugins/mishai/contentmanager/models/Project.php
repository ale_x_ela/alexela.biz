<?php namespace Mishai\ContentManager\Models;

use Model;

/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    // public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mishai_contentmanager_projects';


    public $attachMany = [ 'images' => 'System\Models\File' ];
    public $attachOne = [ 'thumb' => 'System\Models\File' ];
}
