<?php namespace Mishai\ContentManager\Models;

use Model;

/**
 * Model
 */
class Manufacturer extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    // public $timestamps = false;
    protected $jsonable = ['services'];

    /**
     * @var array Validation rules
     */
    public $rules = [];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mishai_contentmanager_manufacturers';

    public $attachMany = [ 'images' => 'System\Models\File' ];
    public $attachOne = [ 'logo' => 'System\Models\File' ];


    public function getCategoryAttribute($cat){
        $value = array_get($this->attributes,'category');
        return array_get($this->getCategoryOptions(), $value);
    }

    public function getCategoryOptions(){
        return [
            1=> 'CNC machining',
            2=> '3D print',
            3=> 'Laser cutting',
            4=> 'Injection molding',
            5=> 'Bending',
            6=> 'Design',
            7=> 'PCB design',
            8=> 'Welding',
            9=> 'Optics'
        ];
    }

    public function getServicesAttribute($serv){

    }

    // services to be added
    public function getServicesOptions(){
        return [
            1=> 'Turning',
            2=> 'Milling',
            3=> 'Conventional',
            4=> 'Laser cutting',
            5=> 'Waterjet cutting',
            6=> 'Bending',
            7=> 'CNC welding',
            8=> 'Welding',
        ];
    }

}