<?php namespace Mishai\ContentManager;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        // return [];
        return [
            'Mishai\ContentManager\Components\Contentloader' => 'Contentloader',
        ];
    }

    public function pluginDetails()
    {
        return [
            'name'        => 'Content manager',
            'description' => 'No description provided yet...',
            'author'      => 'mi-shai.studio',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerNavigation()
    {
        return [
            'contentmanager' => [
                'label'       => 'Content Manager',
                'url'         => Backend::url('mishai/contentmanager/project'),
                'icon'        => 'icon-leaf',
                'order'       => 500,
                'permissions' => ['mishai.contentmanager.*'],
                
                'sideMenu' => [
                    'project' => [
                        'label' => 'Projects',
                        'icon'  => 'icon-play',
                        'url'   => \Backend::url('mishai/contentmanager/project'),
                        // 'order' => 1000,
                    ],  
                    'manufacturer' => [
                        'label'  => 'Manufacturers',
                        'icon'   => 'icon-play',
                        'url'    => \Backend::url('mishai/contentmanager/manufacturer'),
                        // 'order' => 1100,
                    ],  
                ]
            ]
        ];
    }
}
