<?php namespace Mishai\Contentmanager\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Content;
use Mishai\ContentManager\Models\Project as Projects;
use Mishai\ContentManager\Models\Manufacturer;


class Contentloader extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'contentloader Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function objToArray($object){
        return collect($object)->map(
            function ($value) {
                return collect($value)->toArray();
            })->toArray();
    }

    public function getContent($filename){
        $content = Content::loadCached('alexela.biz', $filename . '.htm');
        return $content;
    }

    public function getProjects(){
        return Projects::all();
    }

    public function onGetProject(){

    }

    public function onGetManufactureres(){

    }



}
