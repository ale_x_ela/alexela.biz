<?php namespace Mishai\Contentmanager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Project Back-end Controller
 */
class Project extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mishai.Contentmanager', 'contentmanager', 'project');
    }
}
