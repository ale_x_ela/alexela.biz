<?php namespace Mishai\Contentmanager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('mishai_contentmanager_comments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('comment');
            $table->string('name', 50);
            $table->string('email', 100);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mishai_contentmanager_comments');
    }
}
