<?php namespace Mishai\ContentManager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class createTableMishaiContentManagerManufacturers extends Migration
{
    public function up()
    {
        Schema::create('mishai_contentmanager_manufacturers', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('description');
            $table->text('address');
            $table->tinyInteger('category');
            $table->string('phone', 30);
            $table->string('email', 100);
            $table->string('website', 100);
            $table->text('services');

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mishai_contentmanager_manufacturers');
    }
}