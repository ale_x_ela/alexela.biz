<?php namespace Mishai\ContentManager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class createTableMishaiContentManagerProjects extends Migration
{
    public function up()
    {
        Schema::create('mishai_contentmanager_projects', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name'); //company name
            $table->text('title');
            $table->text('description');
            
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mishai_contentmanager_projects');
    }
}